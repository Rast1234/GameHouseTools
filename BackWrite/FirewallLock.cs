﻿using System;
using System.Linq;
using log4net;
using NetFwTypeLib;

namespace BackWrite
{
	internal class FirewallLock : IDisposable
	{
		private static readonly string RuleName = $"{AboutBox.AssemblyTitle} BlockDHCP";

		private readonly ILog log;

		public FirewallLock(ILog log)
		{
			this.log = log;
			if (!FirewallEnabled())
			{
				log.Warn("Firewall disabled, can't block DHCP requests!");
				return;
			}
			CreateRule();
			log.Info($"Firewall: added rule [{RuleName}]");
		}

		public void Dispose()
		{
			RemoveRule();
			log.Info($"Firewall: removed rule [{RuleName}]");
		}

		private static void CreateRule()
		{
			// block DHCP: incoming UDP to port 67
			var firewallRule = (INetFwRule) Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FWRule"));
			firewallRule.Action = NET_FW_ACTION_.NET_FW_ACTION_BLOCK;
			firewallRule.Description = $"Automatically created by {AboutBox.AssemblyTitle}, blocks incoming DHCP requests";
			firewallRule.Direction = NET_FW_RULE_DIRECTION_.NET_FW_RULE_DIR_IN;
			firewallRule.Protocol = (int)NET_FW_IP_PROTOCOL_.NET_FW_IP_PROTOCOL_UDP;  // why it's int instead of enum?
			firewallRule.LocalPorts = "67";// set only after Protocol!
			firewallRule.Enabled = true;
			firewallRule.InterfaceTypes = "All";
			firewallRule.Name = RuleName;

			var firewallPolicy = GetPolicy();
			firewallPolicy.Rules.Add(firewallRule);
		}

		public static void RemoveRule()
		{
			var firewallPolicy = GetPolicy();
			firewallPolicy.Rules.Remove(RuleName);
		}

		private static INetFwPolicy2 GetPolicy()
		{
			return (INetFwPolicy2) Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FwPolicy2"));
		}

		public bool FirewallEnabled()
		{
			try
			{
				var firewallPolicy = GetPolicy();
				//read Current Profile Types (only to increase Performace)
				//avoids access on CurrentProfileTypes from each Property
				// does not work! it returns multiple profiles and FirewallEnabled[] throws range exception!
				//var currentProfile = (NET_FW_PROFILE_TYPE2_) firewallPolicy.CurrentProfileTypes;
				//return firewallPolicy.FirewallEnabled[currentProfile];

				var profiles = new[]
				{
					NET_FW_PROFILE_TYPE2_.NET_FW_PROFILE2_DOMAIN,
					NET_FW_PROFILE_TYPE2_.NET_FW_PROFILE2_PRIVATE,
					NET_FW_PROFILE_TYPE2_.NET_FW_PROFILE2_PUBLIC
				};
				return profiles.All(x => firewallPolicy.FirewallEnabled[x]);
			}
			catch (Exception e)
			{
				log.Warn("Exception while reading firewall state. Assuming it's disabled.", e);
				return false;
			}
		}

	}
}