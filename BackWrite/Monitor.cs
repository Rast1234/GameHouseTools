﻿using System;
using System.Diagnostics;
using System.IO;
using System.Security.Principal;
using System.Threading;
using System.Windows.Forms;
using log4net;
using ManyConsole;
using NDesk.Options;

namespace BackWrite
{
	internal class Monitor : Command
	{
		private const string DefaultConfigFileName = "config.json";
		public string ConfigFileName { get; set; } = DefaultConfigFileName;

		public Monitor()
		{
			IsCommand(nameof(Monitor), "Run forever, monitor changes, delete writebacks.");
			HasLongDescription("Run with \"help\" arg to see all commands!\n\nRuns forever with tray icon. Monitors changes on specified paths (see config) and deletes writeback files (see confiig too). Requires NTFS USN and Windows Firewall.");
			HasOption("c|config:", $"Path to config file. Default is [{DefaultConfigFileName}].", x => ConfigFileName = x ?? DefaultConfigFileName);
		}



		private static bool RestartAsAdmin(ILog log)
		{
			if (IsAdministrator() == false)
			{
				log.Warn("Started as regular user, will restart as admin");
				// Restart program and run as admin
				var exeName = Process.GetCurrentProcess().MainModule.FileName;
				var startInfo = new ProcessStartInfo(exeName)
				{
					Verb = "runas"
				};
				Process.Start(startInfo);
				return true;
			}
			log.Info("Running as administrator");
			return false;
		}

		private static bool IsAdministrator()
		{
			var identity = WindowsIdentity.GetCurrent();
			var principal = new WindowsPrincipal(identity);
			return principal.IsInRole(WindowsBuiltInRole.Administrator);
		}

		private static Configuration InitConfiguration(string configFileName)
		{
			var configFile = new FileInfo(configFileName);
			if (!configFile.Exists)
			{
				// nobody will see this because log is not initialized...
				throw new ArgumentException(
					$"Configuration file [{configFile}] not found. Create a file or provide filename as argument");
			}
			var config = Configuration.Init(configFile);
			return config;
		}

		private static void SetupApplication()
		{
			// disable that annoying graphical exception dialog
			Application.SetUnhandledExceptionMode(UnhandledExceptionMode.ThrowException);
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
		}

		public override Result Run()
		{
			// prevent from running multiple instances
			// wait 1 second because another instance might be terminating
			try
			{
				if (!Program.Mutex.WaitOne(TimeSpan.FromSeconds(2), false))
				{
					return Result.Silent;
				}
			}
			catch (Exception e)
			{
				return Result.Error($"Exception while waiting for Mutex:\n\n{e}");
			}

			ILog log = null;
			try
			{
				SetupApplication();
				log = LogConfig.GetFileLog();
				if (RestartAsAdmin(log))
				{
					return Result.Silent;
				}

				var configuration = InitConfiguration(ConfigFileName);
				log.Info($@"Started
Current configuration:
{configuration}");

				Application.Run(new TrayApp(log, configuration));
				log.Info("Application loop exited normally");
				return Result.Silent;
			}
			catch (Exception e)
			{
				log?.Error("Unhandled exception!", e);
				return Result.Error($"Unhandled exception!\n\n{e}");
			}
			finally
			{
				log?.Info("Finished\n\n\n\n");
				Program.Mutex.ReleaseMutex();
			}
		}
	}
}