﻿using System.Collections.Generic;

namespace BackWrite
{
	internal class Config
	{
		public int CheckTimeoutSeconds { get; private set; }
		public List<string> PathsToMonitor { get; private set; }
		public List<string> WritebackLocations { get; private set; }
	}
}