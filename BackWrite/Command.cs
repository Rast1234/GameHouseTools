﻿using System;
using System.Windows.Forms;
using ManyConsole;

namespace BackWrite
{
	internal abstract class Command : ConsoleCommand
	{
		public override int Run(string[] remainingArguments)
		{
			Program.result = Result.Silent;
			var result = Run();
			if (result == Result.Silent)
			{
				return 0;
			}
			// no damn way to return more than int!
			Program.result = result;
			return (int) result.Kind;
		}

		public abstract Result Run();
	}
}