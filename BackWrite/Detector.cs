﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using log4net;

namespace BackWrite
{
	internal class Detector
	{
		private readonly ILog log;
		private readonly int consoleEncoding;
		private readonly Dictionary<string, Tuple<BigInteger, DateTime?>> cache;

		public Detector(ILog log)
		{
			this.log = log;
			consoleEncoding = GetConsoleEncoding();
			log.Debug($"Encoding is [{consoleEncoding}]");
			cache = new Dictionary<string, Tuple<BigInteger, DateTime?>>();
		}

		public bool ChangedRecently(string pathToCheck, DateTime lowerBound, DateTime upperBound)
		{
			// force trailing slash
			pathToCheck = pathToCheck.TrimEnd('\\', '/') + Path.DirectorySeparatorChar;

			var fileInfo = new FileInfo(pathToCheck);

			if (fileInfo.Attributes.HasFlag(FileAttributes.Directory))
			{
				// is dir
				var directoryInfo = new DirectoryInfo(pathToCheck);
				if (directoryInfo.Parent != null)
				{
					log.Warn($"Directory [{directoryInfo.FullName}] is not a drive! Can not check!");
					return false;
				}
				var changeDate = LastChangedDateByUsn(directoryInfo);
				if (changeDate == null)
				{
					log.Warn("Could not get last change date from USN");
					return false;
				}
				var isChanged = IsChanged(changeDate.Value, lowerBound, upperBound);
				if (isChanged)
					log.Info($"Drive changed: [{directoryInfo.FullName}]");
				return isChanged;
			}
			else
			{
				// is file
				var isChanged = IsChanged(fileInfo.LastWriteTime, lowerBound, upperBound);
				if (isChanged)
					log.Info($"File changed: [{fileInfo.FullName}]");
				return isChanged;
			}
		}

		private bool IsChanged(DateTime date, DateTime lowerBound, DateTime upperBound)
		{
			if(date.Kind != DateTimeKind.Local)
				throw new ArgumentException($"Expected kind [{DateTimeKind.Local}], got [{date.Kind}]", nameof(date));
			if (lowerBound.Kind != DateTimeKind.Local)
				throw new ArgumentException($"Expected kind [{DateTimeKind.Local}], got [{lowerBound.Kind}]", nameof(lowerBound));
			if (upperBound.Kind != DateTimeKind.Local)
				throw new ArgumentException($"Expected kind [{DateTimeKind.Local}], got [{upperBound.Kind}]", nameof(upperBound));

			if (date >= upperBound)
			{
				log.Debug($"Too recent changes: [{date}] (range [{lowerBound}]-[{upperBound}])");
				return false;
			}
			if (date <= lowerBound)
			{
				log.Debug($"Too old changes: [{date}] (range [{lowerBound}]-[{upperBound}])");
				return false;
			}
			log.Debug($"Found suitable changes [{date}] (range [{lowerBound}]-[{upperBound}])");
			return true;
		}

		private DateTime? LastChangedDateByUsn(DirectoryInfo drive)
		{
			var queryResult = RunFsutilCommand($"usn queryjournal {drive.FullName.TrimEnd('\\', '/')}");
			var rowsWithNumbers = queryResult.Take(7).ToList();
			if (rowsWithNumbers.Count != 7)
			{
				log.Warn($"USN for drive [{drive.FullName}] is probably disabled or inaccessible, can not detect changes");
				return null;
			}

			var matches = rowsWithNumbers.Select(s => usnOutputPattern.Match(s)).ToList();
			if (matches.Any(m => !m.Success))
			{
				log.Warn($"USN for drive [{drive.FullName}] is probably disabled or inaccessible, can not detect changes");
				return null;
			}

			// nextUsn is useless, because we can't read last using it!
			// but we can walk back...
			var nextUsnHexString = matches[2].Groups[1].Value;
			var nextUsn = BigInteger.Parse(nextUsnHexString, NumberStyles.HexNumber);
			// cached values maybe?
			if (cache.ContainsKey(drive.FullName))
			{
				var value = cache[drive.FullName];
				log.Info($"Found USN-date in cache for drive [{drive.FullName}]: [{value.Item1}],[{value.Item2}]");
				// if nothing changed on drive since last run
				if (value.Item1 == nextUsn)
				{
					log.Info("Nothing changed on drive, using date from cache");
					return value.Item2;
				}
				log.Info($"Drive changed, cache is old");
			}
			else
			{
				log.Info($"Not found in cache: [{drive.FullName}]");
			}

			var dateMaybe = FindLastRecord(drive, nextUsn);
			log.Debug($"Insert into cache: [{drive.FullName}]: [{nextUsn}] [{dateMaybe}]");
			cache[drive.FullName] = new Tuple<BigInteger, DateTime?>(nextUsn, dateMaybe);
			return dateMaybe;

			// slow, CPU heavy, probably IO heavy
			//return FindLastRecordReadWholeUSN(drive);
		}

		private DateTime? FindLastRecordReadWholeUSN(DirectoryInfo drive)
		{
			var queryData = RunFsutilCommand($"usn readjournal {drive.FullName.TrimEnd('\\', '/')} csv", 10);
			return ParseRecordMaybe(queryData.Last());
		}

		private DateTime? FindLastRecord(DirectoryInfo drive, BigInteger nextUsn, int depth=1000)
		{
			for (var attempt=1; attempt < depth+1; attempt++)
			{
				var currentUsn = nextUsn + 1 - attempt;
				var queryData = RunFsutilCommand($"usn readjournal {drive.FullName.TrimEnd('\\', '/')} csv startusn={currentUsn}", 1);
				var date = ParseRecordMaybe(queryData.Last());
				if (date != null)
				{
					log.Info($"Found last USN with {attempt} attempts. Date: [{date}]");
					return date;
				}
			}
			log.Warn("Can't find last USN to parse!");
			return null;
		}

		private DateTime? ParseRecordMaybe(string recordMaybe)
		{
			string dateString = null;
			try
			{
				if(recordMaybe == null)
					throw new ArgumentNullException(nameof(recordMaybe));
				var parts = recordMaybe.Split(',');
				log.Debug($"Trying to parse record:\n\t{string.Join("\n\t", parts)}");
				if (parts.Length < 6)
				{
					throw new InvalidOperationException($"Too short record: only [{parts.Length}] fields");
				}
				dateString = parts[5].Trim('"'); // "06.09.2016 16:34:52"
				log.Debug($"DOES HE LOOK LIKE A DATE?! [{dateString}]");
				var date = DateTime.ParseExact(dateString, "dd.MM.yyyy H:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.AssumeLocal);
				if (date.Kind != DateTimeKind.Local)
					throw new InvalidOperationException($"Bad parsed datetime kind {date.Kind}");
				log.Debug($"Parsed date from USN [{date}]");
				return date;
			}
			catch (Exception e)
			{
				log.Warn($"Error while parsing record [{recordMaybe}] [{dateString}]", e);
				return null;
			}
		}

		private List<string> RunFsutilCommand(string command, int? onlyLastLines = null)
		{
			log.Debug($"Executing [fsutil {command}]");
			var process = new Process
			{
				StartInfo =
				{
					FileName = "fsutil",
					Arguments = command,
					UseShellExecute = false,
					RedirectStandardOutput = true,
					WindowStyle = ProcessWindowStyle.Hidden,
					CreateNoWindow = true,
					StandardOutputEncoding = Encoding.GetEncoding(866),
					//StandardErrorEncoding = Encoding.GetEncoding(866)
				}
			};
			process.Start();
			if (onlyLastLines.HasValue)
			{
				var maxLines = onlyLastLines.Value;
				var lines = new Queue<string>(maxLines);
				var i = 0;
				while (!process.StandardOutput.EndOfStream)
				{
					var currentLine = process.StandardOutput.ReadLine()?.Trim();
					if (!string.IsNullOrWhiteSpace(currentLine))
					{
						lines.Enqueue(currentLine);
					}
					while(lines.Count > maxLines)
					{
						lines.Dequeue();
					}
					i++;
				}
				var output = lines.ToList();
				log.Debug($"Output (last {output.Count} lines of {i}):\n{string.Join("\n", output)}");
				return output;
			}
			else
			{
				var output = process.StandardOutput.ReadToEnd().Split(new[] { "\r\n" }, StringSplitOptions.None).ToList();
				log.Debug($"Output (all {output.Count} lines):\n{string.Join("\n", output)}");
				return output;
			}
		}

		[System.Runtime.InteropServices.DllImport("kernel32.dll")]
		private static extern int GetSystemDefaultLCID();

		private static int GetConsoleEncoding()
		{
			var lcid = GetSystemDefaultLCID();
			var ci = CultureInfo.GetCultureInfo(lcid);
			var page = ci.TextInfo.OEMCodePage;
			return page;
		}

		private readonly Regex usnOutputPattern = new Regex(@"^.+:\s*0x([\da-f]+)$", RegexOptions.Compiled | RegexOptions.IgnoreCase);
	}
}