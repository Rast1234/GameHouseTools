﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using log4net;

namespace BackWrite
{
	internal class TrayApp : ApplicationContext
	{
		private readonly ILog log;
		private readonly Configuration configuration;
		private readonly NotifyIcon trayIcon;
		private readonly Form aboutBox;
		private readonly TaskManager taskManager;

		public TrayApp(ILog log, Configuration configuration)
		{
			this.log = log;
			this.configuration = configuration;
			taskManager = new TaskManager(configuration, log);
			trayIcon = new NotifyIcon
			{
				Icon = Properties.Resources.TrayIcon,
				ContextMenu = new ContextMenu(new[]
				{
					new MenuItem("Delete writebacks", ManualRun),
					new MenuItem("-"),
					new MenuItem("Open logs folder", OpenLogs),
					new MenuItem("Open configuration", OpenConfig),
					new MenuItem("About", About),
					new MenuItem("-"),
					new MenuItem("Exit", Exit)
				}),
				Visible = true
			};
			aboutBox = new AboutBox();
			taskManager.RunForever();
			//taskManager.RunOnce(() => { Exit(null, null); });
		}

		private void Exit(object sender, EventArgs e)
		{
			log.Info(nameof(Exit));
			// We must manually tidy up and remove the icon before we exit.
			// Otherwise it will be left behind until the user mouses over.
			trayIcon.Visible = false;
			Application.Exit();
		}

		private void About(object sender, EventArgs e)
		{
			log.Info(nameof(About));
			aboutBox.Show();
		}

		private void OpenConfig(object sender, EventArgs e)
		{
			log.Info(nameof(OpenConfig));
			SafeOpenPath(configuration.SourceFile.FullName);
		}

		private void OpenLogs(object sender, EventArgs e)
		{
			log.Info(nameof(OpenLogs));
			SafeOpenPath(LogConfig.LogDirectory.FullName);
		}

		private void ManualRun(object sender, EventArgs e)
		{
			log.Info(nameof(ManualRun));
			taskManager.RunOnce(() => MessageBox.Show("Wriebacks deleted", AboutBox.AssemblyTitle, MessageBoxButtons.OK, MessageBoxIcon.Asterisk));
		}

		private void SafeOpenPath(string path)
		{
			log.Info($"{nameof(SafeOpenPath)} [{path}]");
			try
			{
				Process.Start(path);
			}
			catch (Win32Exception ex)
			{
				var message = $"Can not open [{path}]";
				log.Error(message, ex);
				MessageBox.Show($@"{message}
Exception message:
{ex.Message}", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}
	}
}