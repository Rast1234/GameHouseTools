﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using log4net;
using Timer = System.Windows.Forms.Timer;

namespace BackWrite
{
	internal class TaskManager
	{
		private readonly Configuration configuration;
		private readonly ILog log;
		private readonly Timer timer;
		private readonly BackgroundWorker backgroundWorker;
		private readonly Detector detector;
		private readonly Cleaner cleaner;
		private bool onCompleteRunning;
		private Action onComplete;


		public TaskManager(Configuration configuration, ILog log)
		{
			this.configuration = configuration;
			this.log = log;
			detector = new Detector(log);
			cleaner = new Cleaner(log);

			timer = new Timer();
			timer.Tick += TimerTask;

			backgroundWorker = new BackgroundWorker();
			backgroundWorker.DoWork += BackgroundTask;
			backgroundWorker.RunWorkerCompleted += BackgroundTaskCompleted;
		}

		public void RunOnce(Action onComplete)
		{
			log.Debug(nameof(RunOnce));
			if (this.onComplete != null)
			{
				log.Warn("Callback already set");
			}
			else
			{
				this.onComplete = onComplete;
				log.Debug("New callback onComplete");
			}
			RunWorker();
		}

		public void RunForever()
		{
			log.Debug(nameof(RunForever));
			if (timer.Enabled)
			{
				log.Warn("Timer is already enabled");
				return;
			}
			var timeSpan = TimeSpan.FromSeconds(configuration.Config.CheckTimeoutSeconds);
			timer.Interval = (int)timeSpan.TotalMilliseconds;
			timer.Start();
		}

		private void TimerTask(object sender, EventArgs eventArgs)
		{
			timer.Stop();
			log.Debug(nameof(TimerTask));
			RunWorker();
			// timeout in setings might have changed
			var timeSpan = TimeSpan.FromSeconds(configuration.Config.CheckTimeoutSeconds);
			timer.Interval = (int)timeSpan.TotalMilliseconds;
			log.Debug($"Timer interval is {timer.Interval}");
			timer.Start();
		}

		private void RunWorker()
		{
			log.Debug(nameof(RunWorker));
			if (backgroundWorker.IsBusy)
			{
				log.Warn("Task is already running");
				return;
			}
			backgroundWorker.RunWorkerAsync();
		}

		private void BackgroundTask(object sender, DoWorkEventArgs e)
		{
			log.Debug(nameof(BackgroundTask));

			// do the job here
			configuration.Reload(log);
			// important! copy config ref (not Configuration) because it could be reloaded while you read fields!
			var config = configuration.Config;
			var period = TimeSpan.FromSeconds(config.CheckTimeoutSeconds);
			var toCheck = config.PathsToMonitor;
			var dirsToClean = config.WritebackLocations;
			var now = DateTime.Now;
			var lowerBound = now - period - period; // period * 2
			var upperBound = now - period;
			log.Info($"To check:\n\t{string.Join("\n\t", toCheck)}\nTo clean:\n\t{string.Join("\n\t", dirsToClean)}\nPeriod: {lowerBound} - {upperBound}");
			if (onComplete != null)
			{
				log.Info("Forced delete, not checking for changes");
				cleaner.DeleteWriteBacks(dirsToClean);
			}
			else if (toCheck.Any(x => detector.ChangedRecently(x, lowerBound, upperBound)))
			{
				log.Info("Changes detected! Crush! Destroy!");
				using (new FirewallLock(log))
				{
					log.Debug("Waiting after blocking firewall, there might be clients booting");
					Thread.Sleep(TimeSpan.FromMinutes(1));
					cleaner.DeleteWriteBacks(dirsToClean);
				}
			}
			else
			{
				log.Info("Nothing changed recently, come back later!");
			}
		}

		private void BackgroundTaskCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			log.Debug(nameof(BackgroundTaskCompleted));
			if (onCompleteRunning)
			{
				log.Info("Callback is already running");
				return;
			}
			onCompleteRunning = true;
			onComplete?.Invoke();  // it's safe!
			onComplete = null;
			onCompleteRunning = false;
		}


	}
}