﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using log4net;

namespace BackWrite
{
	internal class Cleaner
	{
		private const string FileNamePattern = "*.vhd";
		private static readonly string Extension = ".vhd".ToLowerInvariant();
		private readonly ILog log;

		public Cleaner(ILog log)
		{
			this.log = log;
		}

		public void DeleteWriteBacks(List<string> dirsToClean)
		{
			log.Debug("Listing files...");
			var allImagesByCompname = GetAllVHDs(dirsToClean)
				.GroupBy(fi => fi.Name.Split('-')[0])
				.ToDictionary(g => g.Key, g => g.ToList());

			log.Debug($"All files grouped by computer name:\n{string.Join("\n", allImagesByCompname.Select(kv => $"[{kv.Key}]:\n\t\t{string.Join("\n\t\t", kv.Value.Select(fi => fi.FullName))}"))}");
			foreach (var kv in allImagesByCompname)
			{
				var comp = kv.Key;
				var files = kv.Value;
				try
				{
					var locked = files.Where(IsFileLocked).ToList();
					if (locked.Any())
					{
						log.Info($"Locked files found, skipping [{comp}]:\n\t{string.Join("\n\t", locked)}");
					}
					else
					{
						foreach (var file in files)
						{
#if !DEBUG
							file.Delete();
#endif
							log.Debug($"Deleted: [{file.FullName}]");
						}
						log.Info($"Removed files for [{comp}]");
					}
				}
				catch (Exception e)
				{
					log.Warn($"Exception while removing files for [{comp}]", e);
				}
			}
		}

		private static IEnumerable<FileInfo> GetAllVHDs(IEnumerable<string> toClean)
		{
			return toClean.Select(path => new DirectoryInfo(path))
				.Where(dir => dir.Exists)
				.SelectMany(dir => dir.EnumerateFiles(FileNamePattern)
						.Where(fi => fi.Extension.ToLowerInvariant() == Extension)
				);
		}

		private static bool IsFileLocked(FileInfo file)
		{
			try
			{
				using (file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None)) return false;
			}
			catch (IOException)
			{
				return true;
			}
		}
	}
}