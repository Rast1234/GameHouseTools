﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using System.Windows.Forms;
using ManyConsole;

namespace BackWrite
{
	internal static class Program
	{
		public static readonly Mutex Mutex = new Mutex(false, "{9445FA73-94D2-4094-B5AD-345E2DF5314B}");
		public static Result result;

		[STAThread]
		private static void Main(string[] args)
		{
			try
			{
				var commands = ConsoleCommandDispatcher.FindCommandsInSameAssemblyAs(typeof(Program)).ToList();
				var patchedArgs = InsertDefaultCommandIfNeeded(args, commands);
				var output = new StringWriter();
				ConsoleCommandDispatcher.DispatchCommand(commands, patchedArgs, output);
				if (result != Result.Silent)
				{
					var resultMessage = output + result?.ResultMessage;
					MessageBox.Show(resultMessage, AboutBox.AssemblyTitle, MessageBoxButtons.OK, result?.Kind ?? MessageBoxIcon.Information);
				}
			}
			catch (Exception e)
			{
				MessageBox.Show($"Wild crazy unhandled exception! O_O\n\n{e}", AboutBox.AssemblyTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private static string[] InsertDefaultCommandIfNeeded(string[] args, List<ConsoleCommand> commands)
		{
			if (args.Length == 0)
			{
				// no args, add default command to run

				return new[] {nameof(Monitor)};
			}

			if (args.First().Equals("help", StringComparison.OrdinalIgnoreCase))
			{
				// help requested, don't touch it
				result = Result.Info("");
				return args;
			}

			var x = commands.FirstOrDefault(c => c.Command.Equals(args.First(), StringComparison.OrdinalIgnoreCase));
			if (x == null)
			{
				// random non-command args, add default command and pass them
				var result = new List<string> { nameof(Monitor)};
				result.AddRange(args);
				return result.ToArray();
			}
			// another command, don't touch it
			return args;
		}
	}
}
