﻿using System.Windows.Forms;

namespace BackWrite
{
	internal class Result
	{
		public string ResultMessage { get; }
		public MessageBoxIcon Kind { get; }

		private Result(string resultMessage, MessageBoxIcon kind)
		{
			ResultMessage = resultMessage;
			Kind = kind;
		}

		public static Result Info(string resultMessage)
		{
			return new Result(resultMessage, MessageBoxIcon.Information);
		}

		public static Result Warn(string resultMessage)
		{
			return new Result(resultMessage, MessageBoxIcon.Warning);
		}

		public static Result Error(string resultMessage)
		{
			return new Result(resultMessage, MessageBoxIcon.Error);
		}

		public static Result Silent { get; } = new Result(null, MessageBoxIcon.None);
	}
}