﻿using System;

namespace BackWrite
{
	internal class Cleanup : Command
	{
		public Cleanup()
		{
			IsCommand(nameof(Cleanup), "Remove firewall rule if left after previous run.");
			HasLongDescription("Use if you killed the application and DHCP is still blocked.");
		}

		public override Result Run()
		{
			try
			{
				FirewallLock.RemoveRule();
				return Result.Info("Cleanup completed");
			}
			catch (Exception e)
			{
				return Result.Error($"Exception while Cleanup (maybe firewall is disabled?):\n\n{e}");
			}
		}
	}
}