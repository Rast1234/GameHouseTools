﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using log4net;
using Timer = System.Windows.Forms.Timer;

namespace IdleMiner
{
	internal class TaskManager
	{
		private readonly Configuration configuration;
		private readonly ILog log;
	    private readonly NotifyIcon _trayIcon;
	    private readonly Timer timer;
		private readonly BackgroundWorker backgroundWorker;
		private bool onCompleteRunning;
		private Action onComplete;
	    private readonly GameClassStateManager gameClassStateManager;

	    public bool Enabled
	    {
	        get { return _enabled; }
	        set
	        {
	            _enabled = value;
	            log.Info($"Set Enabled flag: [{_enabled}]");
            }
	    }

	    public TaskManager(Configuration configuration, ILog log)
		{
            this.configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
			this.log = log ?? throw new ArgumentNullException(nameof(log));
            gameClassStateManager = new GameClassStateManager();
		    Enabled = true;

		    timer = new Timer();
			timer.Tick += TimerTask;

			backgroundWorker = new BackgroundWorker();
			backgroundWorker.DoWork += BackgroundTask;
			backgroundWorker.RunWorkerCompleted += BackgroundTaskCompleted;
		}

	    public void RunOnce(Action onComplete)
		{
			log.Debug(nameof(RunOnce));
			if (this.onComplete != null)
			{
				log.Warn("Callback already set");
			}
			else
			{
				this.onComplete = onComplete;
				log.Debug("New callback onComplete");
			}
			RunWorker();
		}

		public void RunForever()
		{
			log.Debug(nameof(RunForever));
			if (timer.Enabled)
			{
				log.Warn("Timer is already enabled");
				return;
			}
			var timeSpan = TimeSpan.FromSeconds(configuration.Config.CheckTimeoutSeconds);
			timer.Interval = (int)timeSpan.TotalMilliseconds;
			timer.Start();
		}

		private void TimerTask(object sender, EventArgs eventArgs)
		{
			timer.Stop();
			log.Debug(nameof(TimerTask));
			RunWorker();
			// timeout in setings might have changed
			var timeSpan = TimeSpan.FromSeconds(configuration.Config.CheckTimeoutSeconds);
			timer.Interval = (int)timeSpan.TotalMilliseconds;
			log.Debug($"Timer interval is {timer.Interval}");
			timer.Start();
		}

		private void RunWorker()
		{
			log.Debug(nameof(RunWorker));
			if (backgroundWorker.IsBusy)
			{
				log.Warn("Task is already running");
				return;
			}
			backgroundWorker.RunWorkerAsync();
		}

		private void BackgroundTask(object sender, DoWorkEventArgs e)
		{
		    try
		    {
		        log.Debug(nameof(BackgroundTask));
		        // do the job here
		        configuration.Reload(log);
		        // important! copy Config ref (not Configuration) because it could be reloaded while you read fields!
		        var config = configuration.Config;

                gameClassStateManager.ReadState(config.GameClassClientLogFile);
		        gameClassStateManager.RebootMaybe(TimeSpan.FromSeconds(config.RebootAfterFreeSeconds), log);
                var canRun = gameClassStateManager.CanRun;
		        var miners = Process.GetProcessesByName(config.MinerProcessName);
		        log.Info($"Can run: [{canRun}], Enabled: [{Enabled}], processes: [{miners.Length}]");
		        if (canRun)
		        {
		            switch (miners.Length)
		            {
		                case 0:
		                    // not running
		                    log.Info($"Start new miner");
		                    MinerManager.RunMiner(config.MinerStartCommand, Enabled, log);
		                    break;
		                case 1:
		                    // running
		                    log.Info($"Already running, do nothing");
		                    break;
		                default:
		                    // running multiple o_O
		                    log.Info($"Running multiple instances, kill all and run new");
		                    MinerManager.KillAll(miners, log);
		                    MinerManager.RunMiner(config.MinerStartCommand, Enabled, log);
		                    break;
		            }
		        }
		        else
		        {
		            switch (miners.Length)
		            {
		                case 0:
		                    // not running
		                    log.Info($"Already stopped, do nothing");
		                    break;
		                default:
		                    // running
		                    log.Info($"Kill miner(s)");
		                    MinerManager.KillAll(miners, log);
		                    break;
		            }
		        }
		    }
		    catch (Exception ex)
		    {
		        log.Warn($"Exception in BackgroundTask:\n{ex}");
		    }
		}

		private void BackgroundTaskCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			log.Debug(nameof(BackgroundTaskCompleted));
			if (onCompleteRunning)
			{
				log.Info("Callback is already running");
				return;
			}
			onCompleteRunning = true;
			onComplete?.Invoke();  // it's safe!
			onComplete = null;
			onCompleteRunning = false;
		}

	    

	    private bool _enabled;
	    // "18.06.2017 22:56:52 ClientInfo.SetClientState old:3 new:3"
    }
}