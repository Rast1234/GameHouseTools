﻿using System;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace IdleMiner
{
	internal class Configuration
	{
		public Config Config { get; private set; }

		public FileInfo SourceFile { get; }

		public Guid RecordedFileHash { get; private set; }


		private Configuration(Config config, FileInfo configFile, Guid hash)
		{
			Config = config;
			SourceFile = configFile;
			RecordedFileHash = hash;
		}

		public static Configuration Init(FileInfo configFile)
		{
			using (var ms = ReadFile(configFile))
			{
				var hash = GetHash(ms);
				var config = ParseData(ms);
				return new Configuration(config, configFile, hash);
			}

		}

		public Configuration Reload(ILog log)
		{
			SourceFile.Refresh();
			using (var ms = ReadFile(SourceFile))
			{
				var newHash = GetHash(ms);
				if (newHash != RecordedFileHash)
				{
					Config = ParseData(ms);
					RecordedFileHash = newHash;
					log.Info($"Configuration updated:\n{this}");
				}
			}
			return this;
		}

		private static MemoryStream ReadFile(FileInfo file)
		{
			using (var stream = file.OpenRead())
			{
				var ms = new MemoryStream();
				stream.CopyTo(ms);
				ms.Seek(0, SeekOrigin.Begin);
				return ms;
			}
		}

		private static Guid GetHash(MemoryStream ms)
		{
			Guid result;
			using (var md5 = MD5.Create())
			{
				result = new Guid(md5.ComputeHash(ms));
			}
			ms.Seek(0, SeekOrigin.Begin);
			return result;
		}

		private static Config ParseData(MemoryStream ms)
		{
			using (var reader = new StreamReader(ms))
			{
				var data = reader.ReadToEnd();
				// TODO: how to handle default values missing from config?
				// maybe just DON'T USE DEFAULT VALUES?
				return JsonConvert.DeserializeObject<Config>(data, PrivateSetterContractResolver.JsonSerializerSettings);
			}
		}

		public override string ToString() => JsonConvert.SerializeObject(this, Formatting.Indented);

		private class PrivateSetterContractResolver : DefaultContractResolver
		{
			public static readonly JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings
			{
				ContractResolver = new PrivateSetterContractResolver()
			};

			protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
			{
				var jProperty = base.CreateProperty(member, memberSerialization);
				if (jProperty.Writable)
					return jProperty;

				jProperty.Writable = IsPropertyWithSetter(member);

				return jProperty;
			}
			private static bool IsPropertyWithSetter(MemberInfo member)
			{
				var property = member as PropertyInfo;

				return property?.GetSetMethod(true) != null;
			}
		}

	}
}
