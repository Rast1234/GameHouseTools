﻿using System.IO;
using log4net;
using log4net.Appender;
using log4net.Core;
using log4net.Layout;
using log4net.Repository.Hierarchy;

namespace IdleMiner
{
	public class LogConfig
	{
		public static DirectoryInfo LogDirectory = new DirectoryInfo("logs");
		public static ILog GetFileLog()
		{
			var hierarchy = (Hierarchy)LogManager.GetRepository();

			var patternLayout = new PatternLayout
			{
				ConversionPattern = "%date %-5level [%logger] %message%newline"
			};
			patternLayout.ActivateOptions();

			var roller = new RollingFileAppender
			{
				LockingModel = new FileAppender.MinimalLock(),
				AppendToFile = true,
				File = LogDirectory.FullName + Path.DirectorySeparatorChar,
				DatePattern = $"'{typeof(Program).Namespace}'-yyyy-MM-dd'.log'",
				Layout = patternLayout,
				MaxSizeRollBackups = 20,
				MaximumFileSize = "100MB",
				RollingStyle = RollingFileAppender.RollingMode.Composite,
				StaticLogFileName = false
			};
			roller.ActivateOptions();
			hierarchy.Root.AddAppender(roller);

#if DEBUG
			hierarchy.Root.Level = Level.All;
#else
			hierarchy.Root.Level = Level.Info;
#endif
			hierarchy.Configured = true;

			return LogManager.GetLogger(typeof(Program).Namespace);
		}
	}
}