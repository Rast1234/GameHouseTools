﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using log4net;

namespace IdleMiner
{
    public static class MinerManager
    {
        // shit why am i doing this?!
        internal const int CTRL_C_EVENT = 0;
        [DllImport("kernel32.dll")]
        internal static extern bool GenerateConsoleCtrlEvent(uint dwCtrlEvent, uint dwProcessGroupId);
        [DllImport("kernel32.dll", SetLastError = true)]
        internal static extern bool AttachConsole(uint dwProcessId);
        [DllImport("kernel32.dll", SetLastError = true, ExactSpelling = true)]
        internal static extern bool FreeConsole();
        [DllImport("kernel32.dll")]
        static extern bool SetConsoleCtrlHandler(ConsoleCtrlDelegate HandlerRoutine, bool Add);
        // Delegate type to be used as the Handler Routine for SCCH
        delegate Boolean ConsoleCtrlDelegate(uint CtrlType);

        public static void KillAll(IEnumerable<Process> processes, ILog log)
        {
            var i = 0;
            foreach (var process in processes)
            {
                try
                {
                    if (!KillProcessGently(process))
                    {
                        log.Info($"Ctrl-C failed for process [{process}], killing it the hard way...");
                        process.Kill();
                    }
                    i++;
                }
                catch (Exception e)
                {
                    log.Warn($"Exception while killing process [{process}] (PID {process.Id}):\n{e}");
                }
            }
            log.Info($"Killed {i} processes");
        }

        private static bool KillProcessGently(Process process)
        {
            if (AttachConsole((uint)process.Id))
            {
                SetConsoleCtrlHandler(null, true);
                try
                {
                    if (!GenerateConsoleCtrlEvent(CTRL_C_EVENT, 0))
                        return false;
                    process.WaitForExit();
                }
                finally
                {
                    FreeConsole();
                    SetConsoleCtrlHandler(null, false);
                }
                return true;
            }
            return false;
        }

        public static void RunMiner(string command, bool enabled, ILog log)
        {
            if (!enabled)
            {
                log.Info($"Enabled is false, not running miner");
                return;
            }
            try
            {
                var name = GetCompname() + "_" + GpuInfoReader.GetNameAndRam(log);
                
                var startInfo = new ProcessStartInfo
                {
                    UseShellExecute = false,
                    FileName = command,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    CreateNoWindow = true
                };

#if DEBUG
                startInfo.WindowStyle = ProcessWindowStyle.Normal;
                startInfo.CreateNoWindow = false;
#endif

                startInfo.EnvironmentVariables["MINERNAME"] = name;

                startInfo.EnvironmentVariables["GPU_FORCE_64BIT_PTR"] = "0";
                startInfo.EnvironmentVariables["GPU_MAX_HEAP_SIZE"] = "100";
                startInfo.EnvironmentVariables["GPU_USE_SYNC_OBJECTS"] = "1";
                startInfo.EnvironmentVariables["GPU_MAX_ALLOC_PERCENT"] = "100";
                startInfo.EnvironmentVariables["GPU_SINGLE_ALLOC_PERCENT"] = "100";

                Process.Start(startInfo);
            }
            catch (Exception e)
            {
                log.Warn($"Exception while starting [{command}]:\n{e}");
            }
        }

        public static string GetCompname()
        {
            var compname = Environment.GetEnvironmentVariable("COMPUTERNAME");
            return string.IsNullOrEmpty(compname)
                ? "noname"
                : compname.Replace(' ', '_');
        }
    }
}