﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using log4net;

namespace IdleMiner
{
    public static class GpuInfoReader
    {
        public static string GetNameAndRam(ILog log)
        {
            // https://stackoverflow.com/questions/29667666/how-get-gpu-information-in-c

            try
            {
                var objvide = new ManagementObjectSearcher("select * from Win32_VideoController");
            
                foreach (var obj in objvide.Get())
                {
                    var name = obj["Name"]
                        .ToString()
                        .Replace(' ', '_');
                    var ram = double.Parse(obj["AdapterRAM"].ToString())/1024/1024/1024;
                    var ramStr = ram.ToString("F1").Replace(',', '.');
                    return $"{name}_{ramStr}Gb";
                }
                throw new NotImplementedException();
            }
            catch (Exception e)
            {
                log.Warn($"Exception while reading GPU properties, will return 'unknown' :\n{e}");
                return "unknown";
            }
        }
    }
}
