using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using log4net;

namespace IdleMiner
{
    public class GameClassStateManager
    {
        public void ReadState(string filename)
        {
            using (var file = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (var reader = new StreamReader(file))
                {
                    CanRun = false;

                    var data = reader.ReadToEnd().Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

                    var lastStateString = data.Reverse().Select(s => _stateRegex.Match(s)).FirstOrDefault(m => m.Success);
                    if (lastStateString != null)
                    {
                        var oldState = int.Parse(lastStateString.Groups[1].Value);
                        var newState = int.Parse(lastStateString.Groups[2].Value);

                        if (newState == 1 && FreeSince == DateTime.MaxValue)
                        {
                            CanRun = true;
                        }

                        if ((oldState == 3 || oldState == 2) && newState == 1)
                        {
                            // reboot after some time
                            FreeSince = DateTime.Now;
                        }
                        else if (oldState == 1 && newState != 1)
                        {
                            // do not reboot
                            FreeSince = DateTime.MaxValue;
                        }
                    }
                }
            }
        }

        public void RebootMaybe(TimeSpan delay, ILog log)
        {
            var now = DateTime.Now;
            var rebootAt = FreeSince != DateTime.MaxValue 
                ? FreeSince + delay 
                : DateTime.MaxValue;
            if (now < rebootAt)
            {
                return;
            }
            try
            {
                log.Info($"Rebooting, client is free since {FreeSince}");
                var startInfo = new ProcessStartInfo
                {
                    UseShellExecute = false,
                    FileName = @"shutdown /r /f /t 0",
                    WindowStyle = ProcessWindowStyle.Hidden,
                    CreateNoWindow = true
                };
#if DEBUG
                log.Debug("nah, just kidding, it's DEBUG build");
                return;
#endif
                Process.Start(startInfo);
            }
            catch (Exception e)
            {
                log.Warn($"Exception while rebooting:\n{e}");
            }
        }

        public bool CanRun { get; private set; }
        private DateTime FreeSince { get; set; } = DateTime.MaxValue;

        /*
         turn off if:
         * pc was in use
         * pc is free for X seconds
         
         trigger if:
         2 -> 1
         3 -> 1
         
         cancel if:
         1 -> NOT 1
         
             */

        private readonly Regex _stateRegex = new Regex(@"^.+?ClientInfo\.SetClientState old:([\-\d]+) new:([\-\d]+)$", RegexOptions.IgnoreCase | RegexOptions.Compiled);
    }
}