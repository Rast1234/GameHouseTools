﻿using System;
using System.Diagnostics;
using System.IO;
using System.Security.Principal;
using System.Windows.Forms;
using log4net;

namespace IdleMiner
{
	internal class Test : Command
	{
		public Test()
		{
			IsCommand(nameof(Test), "Testing.");
			HasLongDescription("Use for testing. No description. It will burn your house. Probably.");
		}

		public override Result Run()
		{
			// prevent from running multiple instances
			// wait 1 second because another instance might be terminating
			try
			{
				if (!Program.Mutex.WaitOne(TimeSpan.FromSeconds(2), false))
				{
					return Result.Silent;
				}
			}
			catch (Exception e)
			{
				return Result.Error($"Exception while waiting for Mutex:\n\n{e}");
			}

			ILog log = null;
			try
			{
				log = LogConfig.GetFileLog();

				if (RestartAsAdmin())
				{
					return Result.Silent;
				}

                // insert somethng here for testing
			    var name = MinerManager.GetCompname() + "_" + GpuInfoReader.GetNameAndRam(log);
			    MessageBox.Show(name);

                MinerManager.RunMiner(InitConfiguration(IdleMine.DefaultConfigFileName).Config.MinerStartCommand, true, log);

                return Result.Silent;
                
				return Result.Info("What are you doing here? Go away!");
			}
			catch (Exception e)
			{
				return Result.Error($"Unhandled exception!\n\n{e}");
			}
			finally
			{
				Program.Mutex.ReleaseMutex();
			}
		}

		private static Configuration InitConfiguration(string configFileName)
		{
			var configFile = new FileInfo(configFileName);
			if (!configFile.Exists)
			{
				// nobody will see this because log is not initialized...
				throw new ArgumentException(
					$"Configuration file [{configFile}] not found. Create a file or provide filename as argument");
			}
			var config = Configuration.Init(configFile);
			return config;
		}

		private static bool RestartAsAdmin()
		{
			if (IsAdministrator() == false)
			{
				// Restart program and run as admin
				var exeName = Process.GetCurrentProcess().MainModule.FileName;
				var startInfo = new ProcessStartInfo(exeName)
				{
					Verb = "runas"
				};
				Process.Start(startInfo);
				return true;
			}
			return false;
		}

		private static bool IsAdministrator()
		{
			var identity = WindowsIdentity.GetCurrent();
			var principal = new WindowsPrincipal(identity);
			return principal.IsInRole(WindowsBuiltInRole.Administrator);
		}

	}
}