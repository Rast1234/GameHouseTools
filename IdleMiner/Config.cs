﻿namespace IdleMiner
{
	internal class Config
	{
		public int CheckTimeoutSeconds { get; private set; }
		public string GameClassClientLogFile { get; private set; }
        public string MinerStartCommand { get; private set; }
        public string MinerProcessName { get; private set; }
        public int RebootAfterFreeSeconds { get; private set; }
    }
}